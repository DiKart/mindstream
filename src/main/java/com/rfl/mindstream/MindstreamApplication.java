package com.rfl.mindstream;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MindstreamApplication {

	public static void main(String[] args) {
		SpringApplication.run(MindstreamApplication.class, args);
	}
}
