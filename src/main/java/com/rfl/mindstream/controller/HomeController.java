package com.rfl.mindstream.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {
    @RequestMapping("/")
    public String home(Model model){
        return "index";
    }
    @RequestMapping("/kontakte")
    public String kontakte(Model model){
        return "kontakte";
    }
}
